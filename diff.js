'use strict'

const template = require('./config_template')
const config = require('./config')
let failedCount = 0

console.log('starting . . .')
console.log('----------------------------------------')

function printError(configNode, templateNode, cause, key) {
  ++failedCount
  console.log(`[ ${key} ]: ${cause}`)
  console.log('configNode: ', configNode)
  console.log('templateNode: ', templateNode)
  console.log('----------------------------------------')
}

function diff(_config, _template, configKey) {
  for (let key in _template) {
    const templateNode = _template[key]
    const configNode = _config[key]

    
    if (typeof(templateNode) === 'object') {
      configKey = `${configKey}.${key}`
      
      if (configNode === undefined) {
        printError(configNode, templateNode, 'missing', configKey)
      }
      // else if (typeof(configNode) !== 'object') {
      //   printError(configNode, templateNode, 'typeof unmatch', configKey)
      // }
      else {
        diff(configNode, templateNode, configKey)
      }
      const reverseString = configKey.split('').reverse().join('')
      const firstDotIdx = reverseString.indexOf('.') + 1
      const truncateString = reverseString.substring(firstDotIdx)
      configKey = truncateString.split('').reverse().join('')
    }
    else {
      const textNode = `${configKey}.${key}`

      if (configNode === undefined) {
        printError(configNode, templateNode, 'missing', textNode)
      }
      // else if (typeof(configNode) !== typeof(templateNode)) {
      //   printError(configNode, templateNode, 'typeof unmatch', textNode)
      // }
      // else if (configNode !== templateNode) {
      //   printError(configNode, templateNode, 'value unmatch', textNode)
      // }
    }
  }
}

diff(config, template, 'config')

console.log('=======================================')
console.log('============ RESULT SUMARY ============')
console.log('=======================================')

if (failedCount === 0) {
  console.log('perfect!')
}
else {
  console.log('your config file is in bad situation!')
  console.log(`missing ${failedCount} ${failedCount === 1 ? 'item' : 'items'} !!!!!!!!!`)
}
console.log('----------------------------------------')
console.log('done')