'use strict'

const fs = require('fs')
const config = require('./config')
const sprintfJs = require('sprintf-js')

const envTemplatePath = process.argv.slice(2)[0]
let envFile = undefined

if (envTemplatePath[0] === '%') {
  envFile = sprintfJs.sprintf(envTemplatePath, config)
}
else {
  const template = fs.readFileSync(envTemplatePath).toString()
  envFile = sprintfJs.sprintf(template, config)
}
console.log(envFile.trim())